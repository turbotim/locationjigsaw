//
//  LocationProvider.swift
//  Flickr
//
//  Created by Tim Harrison on 11/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import CoreLocation

class LocationProvider: NSObject, CLLocationManagerDelegate {

    typealias LocationCompletion = (CLLocation?) -> Void
    private let locationManager = CLLocationManager()
    private var completions = [LocationCompletion]()
    
    func currentLocation(completion:@escaping LocationCompletion) {
        
        completions.append(completion)
        locationManager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
        case .denied:
            completion(nil)
            completions = [LocationCompletion]()
            break
        case .restricted:
            completion(nil)
            completions = [LocationCompletion]()
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedAlways:
            locationManager.requestLocation()
            break
        case .authorizedWhenInUse:
            locationManager.requestLocation()
            break
        }
    }
    
    //MARK: CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        let location = locations.first
            
        for completion in completions {
            completion(location)
        }
        completions = [LocationCompletion]()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        for completion in completions {
            completion(nil)
        }
        completions = [LocationCompletion]()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationManager.requestLocation()
    }
}
