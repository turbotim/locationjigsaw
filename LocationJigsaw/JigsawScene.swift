//
//  JigsawScene.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 11/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import SpriteKit
import GameplayKit

enum GameState {
    case preGame
    case waitingForMove
    case movingPiece
    case puttingDownPiece
    case hasWon
}

class JigsawScene: SKScene {
    
    private let rows: Int
    private let columns: Int
    private var movingPiece: SKNode?
    private var movingPieceStartGridPosition: SKNode?
    private let jigsawData: JigsawData
    private let label: SKLabelNode
    private let moveDuration: TimeInterval = 0.15
    private let scaleDuration: TimeInterval = 0.05
    private let largeScale: CGFloat = 1.2
    private let normalScale: CGFloat = 1.0
    private var state = GameState.preGame {
        didSet {
            switch state {
            case .preGame, .puttingDownPiece, .hasWon:
                isUserInteractionEnabled = false
                break
            case .waitingForMove, .movingPiece:
                isUserInteractionEnabled = true
                break
            }
        }
    }
    
    init(size: CGSize, image: UIImage, rows: Int, columns: Int) {
        
        self.rows = rows
        self.columns = columns
        let gridPadding = 40
        let gridWidth = size.width - CGFloat(gridPadding)
        let gridRect = CGRect(x: CGFloat(gridPadding / 2), y: gridWidth / 2, width: gridWidth, height: gridWidth)
        self.jigsawData = JigsawData(image: image, gridRect: gridRect, rows: rows, columns: columns)
        self.label = SKLabelNode(fontNamed: "Chalkduster")
        self.label.text = "Remember the Photo"
        self.label.fontSize = 25
        self.label.fontColor = SKColor.white
        self.label.position = CGPoint(x: size.width / 2, y: size.height - 100)
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        state = .preGame
        backgroundColor = SKColor.black
        jigsawData.generateGrid()
        
        addChild(label)
        
        for gridPosition in jigsawData.gridPositions {
            addChild(gridPosition)
        }
        
        for piece in jigsawData.puzzlePieces {
            addChild(piece)
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            self.shufflePieces(count: 10, completion: {
                self.label.text = "Fix the Photo"
                self.state = .waitingForMove
            })
        }
    }
    
    //MARK: Piece Manipulation
    func shufflePieces(count: Int, completion: @escaping () -> Void) {
        
        if count == 0 {
            completion()
            return
        }
        
        let piece = jigsawData.randomPiece()
        let targetGridPosition = jigsawData.randomGridPosition()
        let pieceStartPosition = jigsawData.gridPosition(atPoint: piece.position)!
        
        pickUp(piece: piece, startPosition: pieceStartPosition, touchPoint: piece.position) {
            piece.run(SKAction.move(to: targetGridPosition.position, duration: self.moveDuration)) {
                self.putDown(piece: piece, targetPosition: targetGridPosition, fromPosition: pieceStartPosition) {
                    self.shufflePieces(count: count - 1, completion: completion)
                }
            }
        }
    }
    
    func putDown(piece: SKNode, targetPosition: SKNode, fromPosition : SKNode, completion: @escaping () -> Void) {
        
        if targetPosition == fromPosition {
            putBack(piece: piece, targetPosition: fromPosition, completion: { 
                completion()
            })
            return
        }
        
        guard let switchPiece = self.jigsawData.puzzlePiece(atPoint: targetPosition.position) else {
            assertionFailure("There should always be a switch piece")
            return
        }
        let moveAction = SKAction.move(to: targetPosition.position, duration: self.moveDuration)
        let shrinkAction = SKAction.scale(to: self.normalScale, duration: self.scaleDuration)
        let lowZAction = SKAction.run { piece.zPosition = 0 }
        
        piece.run(SKAction.sequence([moveAction, shrinkAction, lowZAction])) {
            
            let highZAction = SKAction.run { switchPiece.zPosition = 1 }
            let pickUpAction = SKAction.scale(to: self.largeScale, duration: self.scaleDuration)
            let moveAction = SKAction.move(to: fromPosition.position, duration: self.moveDuration)
            let putDownAction = SKAction.scale(to: self.normalScale, duration: self.scaleDuration)
            let lowZAction = SKAction.run { switchPiece.zPosition = 0 }
            switchPiece.run(SKAction.sequence([highZAction, pickUpAction, moveAction, putDownAction, lowZAction])) {
                completion()
            }
        }
    }
    
    func putBack(piece: SKNode, targetPosition: SKNode, completion: @escaping () -> Void) {
        
        let move = SKAction.move(to: targetPosition.position, duration: self.moveDuration)
        let shrink = SKAction.scale(to: self.normalScale, duration: self.scaleDuration)
        piece.run(SKAction.sequence([move, shrink])) {
            piece.zPosition = 0
            completion()
        }
    }
    
    func pickUp(piece: SKNode, startPosition: SKNode, touchPoint: CGPoint, completion: @escaping () -> Void) {
        
        piece.zPosition = 1
        
        let move = SKAction.move(to: touchPoint, duration: self.moveDuration)
        let grow = SKAction.scale(to: self.largeScale, duration: self.scaleDuration)
        
        piece.run(SKAction.sequence([move, grow])) {
            completion()
        }
    }
    
    //MARK: Handle touch logic
    func touchDown(atPoint pos : CGPoint) {
        
        guard let puzzlePiece = jigsawData.puzzlePiece(atPoint: pos),
            let gridPosition = jigsawData.gridPosition(atPoint: pos),
            state == .waitingForMove else {
                return
        }
        
        state = .movingPiece
        movingPieceStartGridPosition = gridPosition
        movingPiece = puzzlePiece
        pickUp(piece: puzzlePiece, startPosition: gridPosition, touchPoint: pos, completion: {})
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
        guard let movingPiece = movingPiece,
            let movingPieceStartGridPosition = movingPieceStartGridPosition,
            state == .movingPiece else {
                return
        }
        
        state = .puttingDownPiece
        
        guard let targetGridPosition = jigsawData.gridPosition(atPoint: pos) else {
            putBack(piece: movingPiece, targetPosition: movingPieceStartGridPosition, completion: {
                self.state = .waitingForMove
            })
            self.movingPiece = nil
            return
        }
        
        putDown(piece: movingPiece, targetPosition: targetGridPosition, fromPosition: movingPieceStartGridPosition, completion: {
            
            self.state = .waitingForMove
            
            if self.jigsawData.hasWon() {
                self.state = .hasWon
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2, execute: {                     
                    let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
                    let gameOverScene = GameOverScene(size: self.size, won: true)
                    self.view?.presentScene(gameOverScene, transition: reveal)
                })
            }
        })
        self.movingPiece = nil
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        
        if let movingPiece = movingPiece {
            movingPiece.position = pos
        }
    }
    
    
    //MARK: Recieve touches and normalize
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchDown(atPoint: touches.first!.location(in: self))
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchMoved(toPoint: touches.first!.location(in: self))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchUp(atPoint: touches.first!.location(in: self))
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.touchUp(atPoint: touches.first!.location(in: self))
    }
}
