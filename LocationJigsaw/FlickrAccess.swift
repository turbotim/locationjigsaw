//
//  Flickr.swift
//  Flickr
//
//  Created by Tim Harrison on 10/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import Foundation
import UIKit

struct FlickrImage {

    let imageId: String
    let imageURL: URL
}

struct FlickrLocation {
    
    let lat: Double
    let lng: Double
}

class FlickrAccess {
    
    let apiKey: String
    let flickrParser = FlickrParser()
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    func fetchImage(flickrLocation: FlickrLocation, completion: @escaping (UIImage?) -> Void) {
        
        flickrImages(location: flickrLocation) { (flickrImages) in
            
            guard let flickrImage = flickrImages?.first else {
                completion(nil)
                return
            }
            
            let imageTask = URLSession.shared.dataTask(with: flickrImage.imageURL, completionHandler: { (data, response, error) in
                if let data = data,
                    let image = UIImage(data: data) {
                    completion(image)
                } else {
                    completion(nil)
                }
            })
            imageTask.resume()
        }
    }
    
    func flickrImages(location: FlickrLocation, completion: @escaping ([FlickrImage]?) -> Void) {
        
        let url = searchUrl(location: location)
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {
                completion(nil)
                return
            }
            
            do {
                guard let jsonResponse = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
                    completion(nil)
                    return
                }
                completion(self.flickrParser.parseFlickrImages(jsonResponse: jsonResponse))
            } catch {
                completion(nil)
                return
            }
        }
        
        task.resume()
    }
    
    private func searchUrl(location: FlickrLocation) -> URL{
        
        let urlString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&sort=interestingness-desc&tags=landmark,statue,sunset,architecture,famous&format=json&nojsoncallback=1&content_type=1&api_key=\(apiKey)&lat=\(location.lat)&lon=\(location.lng)"
        return URL(string:urlString)!
    }
}

class FlickrParser {

    func parseFlickrImages(jsonResponse: [String:Any]) -> [FlickrImage]? {
        
        guard let photoInfo = jsonResponse["photos"] as? [String : Any],
            let photos =  photoInfo["photo"] as? [[String : Any]] else {
                return nil
        }
        
        var photoURLs = [FlickrImage]()
        
        for photo in photos {
            
            guard let farmId = photo["farm"] as? Int,
                let serverId = photo["server"] as? String,
                let imageId = photo["id"] as? String,
                let secret = photo["secret"] as? String else {
                    continue
            }
            
            let urlString = "https://farm\(farmId).staticflickr.com/\(serverId)/\(imageId)_\(secret)_b.jpg"
            let url = URL(string: urlString)!
            photoURLs.append(FlickrImage(imageId: imageId, imageURL: url))
        }
        
        return photoURLs
    }
}
