//
//  PhotoDataProvider.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 11/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import UIKit

class PhotoDataProvider {
    
    var imageSegments = [[UIImage]]()
    let rows: Int
    let columns: Int
    
    init(image: UIImage, rows: Int, columns: Int) {
        
        guard image.size.width > 0 && image.size.height > 0 else {
            assertionFailure("Image must have greater than 0 size")
            self.rows = 0
            self.columns = 0
            return
        }
        
        let imageEdge = image.size.width > image.size.height ? image.size.height : image.size.width
        let croppedImage = UIImage(cgImage: image.cgImage!.cropping(to: CGRect(x: 0, y: 0, width: imageEdge, height: imageEdge))!)
        self.rows = rows
        self.columns = columns
        let pieceWidth = croppedImage.size.width / CGFloat(rows)
        let pieceSize = CGSize(width: pieceWidth, height: pieceWidth)
        
        for row in 0..<rows {
            
            var rowImages = [UIImage]()
            for column in 0..<columns {
                let rect = CGRect(x: CGFloat(row) * pieceSize.width, y: CGFloat(column) * pieceSize.width, width: pieceSize.width, height: pieceSize.height)
                let cgImage = croppedImage.cgImage!.cropping(to: rect)!
                let imagePiece = UIImage(cgImage: cgImage)
                rowImages.append(imagePiece)
            }
            
            imageSegments.append(rowImages)
        }
    }
    
    func image(row: Int, column: Int) -> UIImage? {
        
        return imageSegments[column][row]
    }
}
