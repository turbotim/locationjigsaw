//
//  JigsawData.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 12/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import SpriteKit

class JigsawData {

    var puzzlePieces = [SKNode]()
    var gridPositions = [SKNode]()
    private let rows: Int
    private let columns: Int
    private let gridRect: CGRect
    private var photoDataProvider: PhotoDataProvider
    
    init(image: UIImage, gridRect: CGRect, rows: Int, columns: Int) {
        
        if columns != rows {
            assertionFailure("Jigsaws must be a square")
        }
        
        self.gridRect = gridRect
        self.rows = rows
        self.columns = columns
        self.photoDataProvider = PhotoDataProvider(image: image, rows: rows, columns: columns)
    }
    
    func generateGrid() {
        
        let pieceEdge = (gridRect.size.width) / CGFloat(columns)
        let gridTemplate = SKShapeNode(rectOf: CGSize(width: pieceEdge, height: pieceEdge), cornerRadius: 10.0)
        gridTemplate.lineWidth = 3.0
        
        for row in 0..<rows {
            for column in 0..<columns {
                guard let image = photoDataProvider.image(row: row, column: column) else {
                    assertionFailure("There should always be an image here")
                    continue
                }
                let positionRow = rows - row
                let positionColumn = column + 1
                let gridPosition = gridTemplate.copy() as! SKShapeNode
                let x = (pieceEdge * CGFloat(positionColumn)) - (pieceEdge / 2) + gridRect.origin.x
                let y = (pieceEdge * CGFloat(positionRow)) - (pieceEdge / 2) + gridRect.origin.y
                gridPosition.position = CGPoint(x: x, y: y)
                gridPositions.append(gridPosition)
                
                let texture = SKTexture(image: image)
                let pieceImage = SKSpriteNode(texture: texture, size: gridPosition.frame.size)
                
                let crop = SKCropNode()
                let mask = SKShapeNode(rectOf: CGSize(width: pieceEdge - CGFloat(2), height: pieceEdge - CGFloat(2.0)), cornerRadius: 10.0)
                mask.fillColor = SKColor.white
                mask.lineWidth = 0.0
                crop.maskNode = mask
                crop.addChild(pieceImage)
                crop.position = CGPoint(x: x, y: y)
                
                puzzlePieces.append(crop)
            }
        }
    }
    
    func hasWon() -> Bool {
        for piece in puzzlePieces {
            let index = puzzlePieces.index(of: piece)
            let gridPosition = gridPositions[index!]
            
            if !gridPosition.contains(piece.position) {
                return false
            }
        }
        return true
    }
    
    func randomPiece() -> SKNode {
        let index = Int(arc4random() % UInt32(puzzlePieces.count))
        return puzzlePieces[index]
    }
    
    func randomGridPosition() -> SKNode {
        let index = Int(arc4random() % UInt32(gridPositions.count))
        return gridPositions[index]
    }

    //MARK: Grid and Piece helpers
    func gridPosition(atPoint pos: CGPoint) -> SKNode? {
        for gridPosition in gridPositions {
            if gridPosition.contains(pos) {
                return gridPosition
            }
        }
        return nil
    }
    
    func puzzlePiece(atPoint pos: CGPoint) -> SKNode? {
        for puzzlePiece in puzzlePieces {
            if puzzlePiece.contains(pos) && puzzlePiece.zPosition == 0 {
                return puzzlePiece
            }
        }
        return nil
    }
}
