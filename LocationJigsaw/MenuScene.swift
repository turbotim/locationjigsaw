//
//  MenuScene.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 12/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import SpriteKit

class MenuScene: SKScene {
    
    let newYork = FlickrLocation(lat: 40.748180, lng: -73.986094)
    let london = FlickrLocation(lat: 51.507451, lng: -0.128419)
    let flickr = FlickrAccess(apiKey: "02f8db4a5a06f7e9fea200713744895a")
    let locationProvider = LocationProvider()
    var myLocationNode: SKLabelNode! = nil
    var newYorkNode: SKLabelNode! = nil
    var londonNode: SKLabelNode! = nil
    var playWithNode: SKLabelNode! = nil
    var loadingNode: SKLabelNode! = nil
    var errorNode: SKLabelNode! = nil
    
    override init(size: CGSize) {
        
        super.init(size: size)
        
        backgroundColor = SKColor.black
        
        let centerX = size.width / 2
        let centerY = size.height / 2
        
        _ = addLabel(text:"Local Jigsaw", fontSize: 40, position: CGPoint(x: centerX, y: size.height - 100))
        _ = addLabel(text:"By Tim Harrison", fontSize: 14, position: CGPoint(x: centerX, y: 40))
        playWithNode = addLabel(text:"Play With:", fontSize: 30, position: CGPoint(x: centerX, y: centerY + 100))
        myLocationNode = addLabel(text:"My Location", fontSize: 20, position: CGPoint(x: centerX, y: centerY + 20))
        newYorkNode = addLabel(text:"New York", fontSize: 20, position: CGPoint(x: centerX, y: centerY - 40))
        londonNode = addLabel(text:"London", fontSize: 20, position: CGPoint(x: centerX, y: centerY - 100))
        loadingNode = addLabel(text: "Loading", fontSize: 40, position: CGPoint(x: centerX, y: centerY))
        loadingNode.isHidden = true
        let pulseUp = SKAction.scale(to: 1.1, duration: 0.5)
        let pulseDown = SKAction.scale(to: 0.8, duration: 0.5)
        let pulse = SKAction.sequence([pulseUp, pulseDown])
        let repeatPulse = SKAction.repeatForever(pulse)
        loadingNode.run(repeatPulse)
        errorNode = addLabel(text: "Couldn't Load Game", fontSize: 40, position: CGPoint(x: centerX, y: centerY))
        errorNode.isHidden = true
    }
    
    func startGame(image: UIImage?) {
        
        guard let image = image else {
            loadFailed()
            return
        }
        
        run(SKAction.run() {
            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            let scene = JigsawScene(size: self.size, image: image, rows: 3, columns: 3)
            self.view?.presentScene(scene, transition:reveal)
        })
    }
    
    func loadLondon() {
        startLoad()
        flickr.fetchImage(flickrLocation: london, completion: { (image) in
            self.startGame(image: image)
        })
    }
    
    func loadNewYork() {
        startLoad()
        flickr.fetchImage(flickrLocation: newYork, completion: { (image) in
            self.startGame(image: image)
        })
    }
    
    func loadMyLocation() {
        startLoad()
        locationProvider.currentLocation(completion: { (location) in
            guard let location = location else {
                self.loadFailed()
                return
            }
            
            let currentLocation = FlickrLocation(lat: location.coordinate.latitude, lng: location.coordinate.longitude)
            self.flickr.fetchImage(flickrLocation: currentLocation, completion: { (image) in
                self.startGame(image: image)
            })
        })
    }
    
    func startLoad() {
        myLocationNode.isHidden = true
        newYorkNode.isHidden = true
        londonNode.isHidden = true
        playWithNode.isHidden = true
        loadingNode.isHidden = false
    }
    
    func loadFailed() {
        loadingNode.isHidden = true
        errorNode.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
            self.errorNode.isHidden = true
            self.myLocationNode.isHidden = false
            self.newYorkNode.isHidden = false
            self.londonNode.isHidden = false
            self.playWithNode.isHidden = false
        }
    }
    
    func addLabel(text: String, fontSize: CGFloat, position: CGPoint) -> SKLabelNode {
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = text
        label.fontSize = fontSize
        label.fontColor = SKColor.white
        label.position = position
        addChild(label)
        return label
    }
    
    func touchUp(atPoint pos: CGPoint) {
     
        if newYorkNode.contains(pos) {
            loadNewYork()
        } else if londonNode.contains(pos) {
            loadLondon()
        } else if myLocationNode.contains(pos) {
            loadMyLocation()
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
