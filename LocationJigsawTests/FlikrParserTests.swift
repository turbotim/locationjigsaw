//
//  FlikrAccessTests.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 14/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import XCTest
@testable import LocationJigsaw

class FlikrParserTests: XCTestCase {
    
    let flickrParser = FlickrParser()
    var flickrImages = [FlickrImage]()
    
    override func setUp() {
        super.setUp()
        flickrImages = flickrParser.parseFlickrImages(jsonResponse: loadFlickrResponse()!)!
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func loadFlickrResponse() -> [String: Any]? {
        
        guard let path = Bundle(for: type(of: self)).path(forResource: "flickrSearch", ofType: "json") else {
            XCTFail("Couldn't find json file")
            return nil
        }
        
        let url = URL(fileURLWithPath: path)
        do {
            let jsonData = try Data(contentsOf: url)
            return try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any]
        } catch {
            XCTFail("Couldn't read json file")
            return nil
        }
    }
    
    func testParses5FlickrPhotos() {
        XCTAssert(flickrImages.count == 5, "Parser should parse 5 flickr images, actually parsed \(flickrImages.count)")
    }
    
    func testParsesFlickrURL() {
        let expectedURL = "https://farm6.staticflickr.com/5328/30956370520_b44a87ac2d_b.jpg"
        let actualURL = flickrImages.first!.imageURL.absoluteString
        XCTAssert(expectedURL == actualURL, "URL should be \(expectedURL) was \(actualURL)")
    }
    
    func testParsesImageId() {
        let expectedImageId = "33501827134"
        let actualImageId = flickrImages[flickrImages.count - 1].imageId
        XCTAssert(expectedImageId == actualImageId, "ImageId should be \(expectedImageId) was \(actualImageId)")
    }
    
}
