//
//  JigsawDataTests.swift
//  LocationJigsaw
//
//  Created by Tim Harrison on 14/05/2017.
//  Copyright © 2017 Tim Harrison. All rights reserved.
//

import XCTest
@testable import LocationJigsaw

class JigsawDataTests: XCTestCase {
    
    let rect = CGRect(x: 0, y: 0, width: 20, height: 20)
    var jigsawData:JigsawData!
    
    override func setUp() {
        super.setUp()
        let image = loadTestImage()!
        jigsawData = JigsawData(image: image, gridRect: rect, rows: 2, columns: 2)
        jigsawData.generateGrid()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func loadTestImage() -> UIImage? {
        
        guard let path = Bundle(for: type(of: self)).path(forResource: "testImage", ofType: "jpg") else {
            XCTFail("Couldn't find image file")
            return nil
        }
        
        let url = URL(fileURLWithPath: path)
        do {
            let image = try Data(contentsOf: url)
            return UIImage(data: image)
        } catch {
            XCTFail("Couldn't read image file")
            return nil
        }
    }
    
    func testNewlyInitializedGridIsInWonState() {
        XCTAssertTrue(jigsawData.hasWon(), "Newly initialized grid should be in won state")
    }
    
    func testMovedPiecesAreNoLongerInWonState() {
        let piece = jigsawData.puzzlePieces[0]
        let gridPosition = jigsawData.gridPositions[1]
        piece.position = gridPosition.position
        XCTAssertFalse(jigsawData.hasWon(), "Once pieces have been moved the state should not be won")
    }
    
    func testFirstPuzzlePiecePositionShouldBeInsideFirstGridPiece() {
        let piece = jigsawData.puzzlePieces[0]
        let gridPosition = jigsawData.gridPositions[0]
        let pieceGridPosition = jigsawData.gridPosition(atPoint: piece.position)
        XCTAssertTrue(gridPosition == pieceGridPosition, "First piece should be located on first grid position")
    }
    
    func testFirstGridPositionShouldBeInsideFirstPuzzlePiece() {
        let piece = jigsawData.puzzlePieces[0]
        let gridPosition = jigsawData.gridPositions[0]
        let foundPiece = jigsawData.puzzlePiece(atPoint: gridPosition.position)
        XCTAssertTrue(piece == foundPiece, "First piece should be located on first grid position")
    }
}
